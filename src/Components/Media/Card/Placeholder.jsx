import React from 'react';
import placeholder from './placeholder.png';

import * as S from './Styles';

const Placeholder = () => <S.Placeholder src={placeholder} alt="Loading..." />;

export { Placeholder, placeholder };
